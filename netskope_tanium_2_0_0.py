"""Script for netskope tanium. v 2.0.0"""
import sys
import logging
import logging.handlers
import argparse
import subprocess
import os
import time

PRE_PATH = None
SLEEP_TIME = 5


def get_platform(logger):
    """Provide OS in which script is running."""
    platforms = {"linux1": "Linux", "linux2": "Linux", "darwin": "MAC", "win32": "Windows"}
    if sys.platform not in platforms:
        return sys.platform
    logger.info("Script is running on {0}.".format(platforms[sys.platform]))
    return platforms[sys.platform]


def process_path(full_path, logger):
    """Process file path for Windows system."""
    import winreg

    global PRE_PATH
    path = ""
    reg_root = {
        "HKEY_CLASSES_ROOT": winreg.HKEY_CLASSES_ROOT,
        "HKEY_CURRENT_USER": winreg.HKEY_CURRENT_USER,
        "HKEY_LOCAL_MACHINE": winreg.HKEY_LOCAL_MACHINE,
        "HKEY_USERS": winreg.HKEY_USERS,
        "HKEY_CURRENT_CONFIG": winreg.HKEY_CURRENT_CONFIG,
    }
    try:
        path_list = full_path.split("\\")
        if path_list[1] not in reg_root:
            logger.error(
                "Error while processing file path. Provide registry path must be like computer\\HKEY_*\\*. "
                "Provided path - {0}".format(full_path)
            )
            raise ValueError("Invalid registry path")
        PRE_PATH = reg_root[path_list[1]]
        path = "\\".join(path_list[2:])
        return path
    except Exception as e:
        logger.error("Error while processing file path. Error - {0}".format(e))
        raise ValueError("Error while processing registry path")


def save_reg(full_path, key, value, logger):
    """Save key into provided registry path.

    Args:
        full_path (str): registry path
        key (str): key to read
        value (str): value
        logger (Logger): Logger object
    """
    import winreg

    global PRE_PATH

    try:
        reg_path = winreg.CreateKeyEx(PRE_PATH, full_path, 0, (winreg.KEY_ALL_ACCESS))  # Get the current user registry
        winreg.SetValueEx(reg_path, key, 0, winreg.REG_SZ, value)  # Updated the key value
        if reg_path:
            winreg.CloseKey(reg_path)
        return True
    except OSError as e:
        logger.error(
            "OSError occurred while writing into registry. Reg_path - {0}, Reg_key - {1}, Error - {2}".format(
                full_path, key, e
            )
        )
    except Exception as e:
        logger.error(
            "Error occurred while writing into registry. Reg_path - {0}, Reg_key - {1}, Error - {2}".format(
                full_path, key, e
            )
        )
    return False


def create_remove_file(full_path, logger):
    """If file exists it will remove that file and file does not exist it will create it.

    Args:
        full_path (str): full file path with filename
        logger (Logger): Logger object
    """
    try:
        if os.path.exists(full_path):
            os.remove(full_path)
            logger.info("File {0} removed".format(full_path))
        else:
            open(full_path, "a").close()
            logger.info("File {0} created".format(full_path))
        return True
    except OSError as e:
        logger.error("OSError occurred while creating/removing file. File path - {0}, Error - {1}".format(full_path, e))
    except Exception as e:
        logger.error("Error occurred while creating/removing file. File path - {0}, Error - {1}".format(full_path, e))
    return None


def init_logger(path):
    """
    Initialize logger object.

    Args:
        path (str): Path for log  file.
    """
    # Initialise logger
    logger = logging.getLogger("netskope_tanium")
    ch = logging.handlers.RotatingFileHandler(path, maxBytes=(10 * 1024 * 1024), backupCount=10)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.setLevel(logging.INFO)
    return logger


def init_parser():
    """
    Initialize Parser for command line args.
    """
    parser = argparse.ArgumentParser()
    # Arg for using either nsdiag or stagevntsvc
    use_group = parser.add_mutually_exclusive_group()
    use_group.add_argument(
        "--use_nsdiag", "-nc", action="store_true", help="Use nsdiag for Netskope Client operations.", default=False
    )
    use_group.add_argument(
        "--use_stagevntsvc",
        "-ns",
        action="store_true",
        help="Use stagevntsvc for Netskope Service operations. Script must be run as administrator to use stagevntsvc.",
        default=True,
    )
    # Arg for set flag or Restart
    usecase_group = parser.add_mutually_exclusive_group()
    usecase_group.add_argument(
        "--set",
        "-s",
        choices=["enable", "disable"],
        help="Provide 'enable' or 'disable' for nsdiag/stagevntsvc script.",
    )
    usecase_group.add_argument(
        "--restart", "-r", action="store_true", help="Restart using nsdiag/stagevntsvc.", default=True
    )
    usecase_group.set_defaults()
    # Arg for file path
    parser.add_argument(
        "--filePath",
        "-fp",
        help="Full File Path with filename or Full Registry Path.",
        default="computer\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Netskope\\Provisioning"
        if sys.platform == "win32"
        else "/Library/Application Support/Netskope/STAgent/data/managed.json",
    )
    # Arg for key and val
    parser.add_argument(
        "--key",
        "-k",
        help="Key to add/update. Only for Windows",
        default="managed",
    )
    parser.add_argument(
        "--value",
        "-v",
        help="Value of the key to be added/updated. Only for Windows",
        default="1",
    )
    # Arg for debugging logs
    parser.add_argument(
        "--debug",
        "-d",
        help="[Optional] Full file path for logging. Default path would be current directory and "
        "file name would be 'netskope_tanium.log'.",
        default="netskope_tanium.log",
    )
    # Arg for netskope executable path
    parser.add_argument(
        "--netskope_path",
        "-nsp",
        help="[Optional] Directory where nsdiag/stagevntsvc is located.",
    )
    return parser


def execute_subprocess(cmd, logger, shell=False):
    """
    Execute sub process according to the args.

    Args:
        cmd: Command to execute.
        logger: Logger object.
        shell: Option for shell.
    """
    logger.info("Executing {0}.".format(cmd))
    subprocess.call(cmd, shell=shell)


def sleep():
    """Sleep for SLEEP_TIME seconds."""
    print("Sleeping for {0} seconds.".format(SLEEP_TIME))
    time.sleep(SLEEP_TIME)


def process_and_execute_command(platform, cmd, args, logger):
    """
    Process and Execute sub process according to the args.

    Args:
        platform (str): OS of the system.
        cmd: Executable's path.
        args: Command line args.
        logger: Logger object.
    """
    if args.set:
        if args.use_nsdiag:
            execute_subprocess(cmd + " -t {0}".format(args.set), logger, (platform == "MAC"))
        else:
            if platform == "Windows":
                execute_subprocess(cmd + " -{0}".format("start" if args.set == "enable" else "stop"), logger)
            else:
                execute_subprocess(
                    "launchctl {0} {1}".format("load" if args.set == "enable" else "unload", cmd), logger, True
                )
    else:
        if args.use_nsdiag:
            execute_subprocess(cmd + " -t disable", logger, (platform == "MAC"))
            sleep()
            execute_subprocess(cmd + " -t enable", logger, (platform == "MAC"))
        else:
            if platform == "Windows":
                execute_subprocess(cmd + " -stop", logger)
                print()
                sleep()
                execute_subprocess(cmd + " -start", logger)
            else:
                print("Disabling Netskope Service.")
                execute_subprocess("launchctl unload {0}".format(cmd), logger, True)
                sleep()
                print("Enabling Netskope Service.")
                execute_subprocess("launchctl load {0}".format(cmd), logger, True)


def execute_win(args, logger):
    """
    Execute script for windows platform.

    Args:
        args: Command line args.
        logger: Logger object.
    """
    key_to_read = args.key
    value_to_write = args.value
    logger.info("Key to add | update : {0}".format(key_to_read))
    logger.info("Value : {0}".format(value_to_write))
    cmd = args.netskope_path
    full_path = args.filePath
    if cmd is None:
        cmd = "C:\\Program Files (x86)\\Netskope\\STAgent\\"
    cmd = os.path.join(cmd, "nsdiag.exe" if args.use_nsdiag else "stAgentSvc.exe")
    full_path = process_path(full_path, logger)
    if save_reg(full_path, key_to_read, value_to_write, logger):
        logger.info("Registry updated successfully.")
        process_and_execute_command("Windows", cmd, args, logger)
    else:
        logger.error("Error while updating registry.")


def execute_mac(args, logger):
    """
    Execute script for MAC platform.

    Args:
        args: Command line args.
        logger: Logger object.
    """
    cmd = args.netskope_path
    full_path = args.filePath
    if cmd is None:
        if args.use_nsdiag:
            cmd = "/Library/Application\\ Support/Netskope/STAgent/"
        else:
            cmd = "/Library/LaunchDaemons/"
    cmd = os.path.join(cmd, "nsdiag" if args.use_nsdiag else "com.netskope.stagentsvc.plist")
    if create_remove_file(full_path, logger) is None:
        logger.error("Error while creating/removing file.")
    process_and_execute_command("MAC", cmd, args, logger)


def main():
    """Entry point of the program."""
    logger = None
    try:
        parser = init_parser()
        args = parser.parse_args()
        logger = init_logger(args.debug)
        platform = get_platform(logger)
        if platform == "Windows":
            execute_win(args, logger)
        elif platform == "MAC":
            execute_mac(args, logger)
        else:
            print("This Script only supports MAC and Windows OS.")
            logger.error("This Script only supports MAC and Windows OS.")
        logger.info("Script execution completed.")
    except Exception as e:
        print("Error occurred. Check logs for more details.")
        logger.error("Error occurred. Error -{0}".format(e))


if __name__ == "__main__":
    main()
